import AppForm from './AppForm'
import MakeStringIntInRangeValidator from './Validators/Closures/MakeStringIntInRangeValidator'
import StringAlphaValidator from './Validators/StringAlphaValidator'
import StringIntConvertibleValidator from './Validators/StringIntConvertibleValidator'
import StringNotNullOrWhitespaceValidator from './Validators/StringNotNullOrWhitespaceValidator'
import { useState } from 'react'
import FormAlert from './FormAlert'

export default function App() {
  
  let [error, setError] = useState("");
  let [output, setOutput] = useState("");

  
  
  function getAgeString(age) {
    if (age < 18) {
      return "niepełnoletni";
    }
    else {
      return "pełnoletni";
    }
  }

  function onFormSubmit(inputName, inputAge) {
    setError("");
    setOutput("");

    let validationFields = [
      {
        name: "Imię", 
        value: inputName,
        validators: [
          StringNotNullOrWhitespaceValidator,
          StringAlphaValidator
        ]
      },
      {
        name: "Wiek", 
        value: inputAge,
        validators : [
            StringNotNullOrWhitespaceValidator,
            StringIntConvertibleValidator,
            MakeStringIntInRangeValidator(0, 200)
        ]
      }
    ];
    
    for (let field of validationFields) {
      for(let validator of field.validators) {
        let result = validator(field.value);
        if (!result.valid) {
          setError(`${field.name}: ${result.error}`);
          return;
        }
      }
    }

    setOutput(`Witaj ${inputName}!, ${getAgeString(inputAge)}.`)
  }

  return (
    <div className='app'>
    <header className='p-3 bg-primary text-light'>
      <h1>Walidacja użytkownika</h1>
    </header>
    <div className='container p-2'>
      <FormAlert error={error}></FormAlert>
      <AppForm onSubmit={onFormSubmit}/>
      <div className='container p-4'>
        <h2>{output}</h2>
      </div>
    </div>
    </div>
  )
}