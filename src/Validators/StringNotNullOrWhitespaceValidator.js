export default function StringNotNullOrWhitespaceValidator(inputString) {
   if (inputString === undefined){ 
       return { valid: false, error: "String is undefined"};
   }
   if (inputString === null){ 
       return { valid: false, error: "String is null"};
   }
   if (inputString.trim().length < 1){ 
       return { valid: false, error: "String is empty"};
   }
   return { valid: true };
}