export default function StringIntConvertibleValidator(inputString) {
    if(isNaN(parseInt(inputString))) {
        return { valid: false, error: "String not convertible to int."};
    }
    return { valid: true };
}