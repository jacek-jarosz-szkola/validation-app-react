export default function MakeStringIntInRangeValidator(min, max) {

    if (isNaN(min) || isNaN(max)) throw new Error('Invalid range arguments');
    
    return (inputString) => {
        let x = parseInt(inputString);
        if (x < min || x > max) {
            return { valid: false, error: "Integer out of range"};
        }
        return { valid: true };
    };
}