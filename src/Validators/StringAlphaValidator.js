export default function StringAlphaValidator(inputString) {

    if(!/^[A-Za-z][A-Za-z ]*$/.test(inputString)) {
        return { valid: false, error: "String contains illegal characters."};
    }
    return {valid: true};
}