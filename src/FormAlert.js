export default function FormAlert(props) {
    if (props.error) {
        return (
            <div class="alert alert-danger" role="alert">
                {props.error}
            </div>
        );
    }
    else {
        return (<></>)
    }

}