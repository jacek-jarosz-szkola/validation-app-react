import { useState } from "react"

export default function AppForm(props) {

    let [name, setName] = useState("");
    let [age, setAge] = useState(0);

    function onNameInputChange(ev) {
        setName(ev.target.value);
    }

    function onAgeInputChange(ev) {
        setAge(ev.target.valueAsNumber.toString());
    }

    function validatePressed(){
        props.onSubmit(name, age);
    }

    return (
        <div id="app-form" className="AppForm form container-fluid">
            <div className="form-group">
                <label htmlFor="nameInput">Imię</label>
                <input type="text" className="form-control" id="nameInput" onInput={onNameInputChange}></input>
            </div>
            <div className="form-group">
                <label htmlFor="ageInput">Wiek</label>
                <input type="number" className="form-control" id="ageInput" onInput={onAgeInputChange}></input>
            </div>
            <button type="submit" className="mt-3 btn btn-block btn-primary" id="validateButton" onClick={validatePressed}>Waliduj</button>
        </div>
    );
}